beehive
=======

Beehive is a torrent tracker written in Python.

I'm a casual torrenter, and I know very little about the protocol, so I don't even know if this'll work.

This is purely a learning experience. I don't really expect this to be production-ready, and I don't really
intend on running a tracker, because the scene is nothing but drama and [Mean Girls][mean-girls-yt].

I get really bored doing things right when I think of something cool, so there's a lot of shortcuts here. For
instance, I know my SECRET_KEY is public right now...I'll probably fix it later.

[mean-girls-yt]: https://www.youtube.com/watch?v=ZxcZmRlDqZ0