import argparse

from django.core.management.base import BaseCommand

from register import utils


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('metainfo_path', nargs='+',
                            type=argparse.FileType('r'),
                            help='The path to a torrent metainfo file')

    def handle(self, *args, **options):
        # We actually take a list of metainfo file paths.
        # Iterate through it.
        for fh in options['metainfo_path']:
            # We use the filename as the torrent's name.
            filename = fh.name.split('/')[-1].split('.torrent')[0]
            self.stdout.write("Adding " + filename)
            try:
                utils.register_torrent(filename, fh.read())
            except Exception as e:
                self.stderr.write("Error adding torrent: " + str(e))
