import datetime
import sha

import bencode

from django.utils import timezone

import models


def register_torrent(name, metainfo_string):
    """Register a torrent with the torrent database.

    Arguments:
    name -- the torrent's 'name'. This is user-defined.
    metainfo -- the metainfo of the torrent to register (a.k.a.
                the '.torrent file')
    """
    # Check that we have everything we need to register.
    if not validate_metainfo(metainfo_string):
        raise StandardError("Invalid torrent file.")

    # Only add the torrent if we don't have it already.
    # otherwise, just update the info.
    info_hash = calculate_info_hash(metainfo_string)

    try:
        t = models.Torrent.objects.get(info_hash=info_hash)
    except models.Torrent.DoesNotExist:
        t = models.Torrent(name=name)

    t.raw_metainfo = metainfo_string
    update_torrent_from_metainfo(t)


def update_torrent_from_metainfo(torrent):
    # Populate fields.
    mi = bencode.bdecode(torrent.raw_metainfo)
    torrent.creation_date = timezone.make_aware(datetime.datetime.fromtimestamp(int(mi['creation date']))) if 'creation date' in mi else None
    torrent.created_by = mi['created by'] if 'created by' in mi else None
    torrent.comment = mi['comment'] if 'comment' in mi else None
    torrent.piece_length = mi['info']['piece length']
    torrent.pieces_hash = mi['info']['pieces']
    torrent.private = True if ('private' in mi['info'] and mi['info']['private'] == 1) else False

    # Gotta save before adding FKs referring to the object.
    torrent.full_clean()
    torrent.save()

    # Populate the torrent's file list
    # First, detect if the torrent is single-file or multi-file.
    if 'name' in mi['info'] and 'length' in mi['info']:
        # torrent is single-file.
        try:
            models.File.objects.get(name=mi['info']['name'], torrent=torrent)
        except models.File.DoesNotExist:
            f = models.File(
                name=mi['info']['name'],
                length=mi['info']['length'],
                torrent=torrent
            )

            f.file_hash = mi['info']['md5sum'] if 'md5sum' in mi['info'] else None
            f.full_clean()
            f.save()
    elif 'files' in mi['info']:
        # torrent is multi-file.
        # get common base path (if any)
        base_path = mi['info']['name'] if 'name' in mi['info'] else ""
        # add each file in torrent to database
        for c_file in mi['info']['files']:
            # build path since it's stored in metainfo weirdly.
            path = "/{base}/{file}".format(
                base=base_path,
                file='/'.join(c_file['path'])
            )
            # now we have the (torrent-unique) path, see if it's already in db.
            try:
                models.File.objects.get(name=path, torrent=torrent)
            except models.File.DoesNotExist:
                # not in db, add it.
                # print "New file: {}".format(path)
                f = models.File(
                    name=path,
                    length=c_file['length'],
                    torrent=torrent
                )

                f.file_hash = c_file['md5sum'] if 'md5sum' in c_file else None

                f.full_clean()
                f.save()


def validate_metainfo(metainfo_string):
    """Verify that the provided metainfo is loosely valid.

    This is a blacklist, not a whitelist. This function only looks for
    some key items.

    Arguments:
    metainfo -- the metainfo of the torrent to verify (a.k.a.
                the '.torrent file')
    """
    valid = True
    metainfo = bencode.bdecode(metainfo_string)

    # Check for the Announce URL.
    if not ('announce' in metainfo or 'announce-list' in metainfo):
        valid = False

    # Check for piece info.
    if not ('piece length' in metainfo['info']
            or 'pieces' in metainfo['info']):
        valid = False

    return valid


def calculate_info_hash(metainfo_string):
    """Calculate the 'info hash' of the Torrent metainfo.

    Arguments:
    metainfo_string -- the metainfo of the torrent to calculate an info hash
                       for.
    """
    # The 'info hash' is a 20-byte SHA1 hash of the value of the info
    # key from the metainfo file.
    #
    # Since the value of the info key from the metainfo file is itself a
    # composite object, you need to serialise it to get the info hash. This
    # means you have to rely on Python's bencode/bdecode module being very
    # strict with its syntax. It seem to work, which is awesome!
    mi = bencode.bdecode(metainfo_string)
    s = sha.new()
    s.update(bencode.bencode(mi['info']))
    return s.digest()
