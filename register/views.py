from django.views import generic

import models


class TorrentDetail(generic.DetailView):
    model = models.Torrent
    slug_field = 'info_hash_hex'


class TorrentList(generic.ListView):
    model = models.Torrent
