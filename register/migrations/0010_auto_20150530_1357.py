# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0009_auto_20150530_1338'),
    ]

    operations = [
        migrations.AlterField(
            model_name='torrent',
            name='info_hash',
            field=models.BinaryField(help_text=b'The SHA1 hash of the info dict from the metainfo file.', unique=True, blank=True),
        ),
        migrations.AlterField(
            model_name='torrent',
            name='info_hash_hex',
            field=models.CharField(help_text=b'The hex-encoded info hash.', unique=True, max_length=100, blank=True),
        ),
    ]
