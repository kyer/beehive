# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0011_auto_20150530_1650'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='file_hash',
            field=models.CharField(help_text=b"The file's MD5 hash (hex. encoded). Blank for directories.", max_length=32, null=True, blank=True),
        ),
    ]
