# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'The name of the file, including the path.', max_length=256)),
                ('length', models.PositiveSmallIntegerField(help_text=b"The file's length, in bytes.")),
                ('file_hash', models.CharField(help_text=b"The file's MD5 hash (hex. encoded). Blank for directories.", max_length=32, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='torrent',
            name='piece_length',
            field=models.PositiveSmallIntegerField(default=1, help_text=b'The length (in bytes) of each torrent piece.'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='torrent',
            name='pieces_hash',
            field=models.TextField(default='N', help_text=b'The concatenation of all piece hash values.'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='torrent',
            name='private',
            field=models.BooleanField(default=False, help_text=b'Is this torrent private? As per BEP-27.'),
            preserve_default=False,
        ),
    ]
