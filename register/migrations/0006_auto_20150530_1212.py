# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0005_auto_20150530_1208'),
    ]

    operations = [
        migrations.AlterField(
            model_name='torrent',
            name='date_created',
            field=models.DateTimeField(help_text=b'The metainfo-reported torrent creation date.', null=True, blank=True),
        ),
    ]
