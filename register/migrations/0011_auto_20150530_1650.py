# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0010_auto_20150530_1357'),
    ]

    operations = [
        migrations.AlterField(
            model_name='torrent',
            name='comment',
            field=models.TextField(help_text=b'The metainfo-reported comment.', null=True, blank=True),
        ),
    ]
