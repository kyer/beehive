# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0012_auto_20150530_1747'),
    ]

    operations = [
        migrations.AddField(
            model_name='torrent',
            name='name',
            field=models.CharField(default='Legacy Torrent', help_text=b"The torrent's 'name'. Torrents don't really have names, so it's just the root file/folder name.", max_length=256),
            preserve_default=False,
        ),
    ]
