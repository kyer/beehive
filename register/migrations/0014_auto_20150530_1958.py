# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0013_torrent_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='trackertier',
            name='torrent',
        ),
        migrations.RemoveField(
            model_name='trackertier',
            name='trackers',
        ),
        migrations.AlterField(
            model_name='torrent',
            name='name',
            field=models.CharField(help_text=b"The torrent's 'name'. Torrents don't really have names, so it's just the original file name.", max_length=256),
        ),
        migrations.DeleteModel(
            name='Tracker',
        ),
        migrations.DeleteModel(
            name='TrackerTier',
        ),
    ]
