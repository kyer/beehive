# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0004_auto_20150530_1129'),
    ]

    operations = [
        migrations.AlterField(
            model_name='torrent',
            name='pieces_hash',
            field=models.BinaryField(help_text=b'The concatenation of all piece hash values.'),
        ),
        migrations.AlterField(
            model_name='torrent',
            name='raw_metainfo',
            field=models.BinaryField(help_text=b'The raw un-edited torrent metainfo file.'),
        ),
    ]
