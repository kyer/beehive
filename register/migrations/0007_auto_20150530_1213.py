# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0006_auto_20150530_1212'),
    ]

    operations = [
        migrations.AlterField(
            model_name='torrent',
            name='created_by',
            field=models.CharField(help_text=b'The metainfo-reported creator (program) of the torrent.', max_length=256, null=True, blank=True),
        ),
    ]
