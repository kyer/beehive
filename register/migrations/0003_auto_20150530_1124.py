# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0002_auto_20150530_1045'),
    ]

    operations = [
        migrations.AddField(
            model_name='file',
            name='torrent',
            field=models.ForeignKey(default=1, to='register.Torrent'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='trackertier',
            name='torrent',
            field=models.ForeignKey(default=1, to='register.Torrent'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='torrent',
            name='comment',
            field=models.TextField(help_text=b'The metainfo-reported comment.', blank=True),
        ),
        migrations.AlterField(
            model_name='torrent',
            name='created_by',
            field=models.CharField(help_text=b'The metainfo-reported creator (program) of the torrent.', max_length=256, blank=True),
        ),
    ]
