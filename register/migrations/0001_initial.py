# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Torrent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(help_text=b'The metainfo-reported torrent creation date.')),
                ('created_by', models.CharField(help_text=b'The metainfo-reported creator (program) of the torrent.', max_length=256)),
                ('comment', models.TextField(help_text=b'The metainfo-reported comment.')),
                ('raw_metainfo', models.TextField(help_text=b'The raw un-edited torrent metainfo file.')),
            ],
        ),
        migrations.CreateModel(
            name='Tracker',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('announce_url', models.CharField(help_text=b'Tracker announce URL.', max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='TrackerTier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tier', models.PositiveSmallIntegerField(help_text=b'The tier number.')),
                ('trackers', models.ManyToManyField(help_text=b'List of trackers.', to='register.Tracker')),
            ],
        ),
    ]
