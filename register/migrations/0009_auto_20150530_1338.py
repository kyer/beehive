# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0008_auto_20150530_1329'),
    ]

    operations = [
        migrations.AlterField(
            model_name='torrent',
            name='info_hash_hex',
            field=models.CharField(help_text=b'The hex-encoded info hash.', max_length=100, blank=True),
        ),
    ]
