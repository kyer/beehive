import binascii

from django.core.urlresolvers import reverse
from django.db import models

import utils


class Torrent(models.Model):
    name = models.CharField(
        max_length=256,
        help_text="The torrent's 'name'. Torrents don't really have names, "
                  "so it's just the original file name."
    )

    date_created = models.DateTimeField(
        blank=True,
        null=True,
        help_text="The metainfo-reported torrent creation date."
    )

    created_by = models.CharField(
        max_length=256,
        blank=True,
        null=True,
        help_text="The metainfo-reported creator (program) of the torrent."
    )

    comment = models.TextField(
        blank=True,
        null=True,
        help_text="The metainfo-reported comment."
    )

    piece_length = models.PositiveSmallIntegerField(
        help_text='The length (in bytes) of each torrent piece.'
    )

    pieces_hash = models.BinaryField(
        help_text='The concatenation of all piece hash values.'
    )

    private = models.BooleanField(
        help_text='Is this torrent private? As per BEP-27.'
    )

    raw_metainfo = models.BinaryField(
        help_text="The raw un-edited torrent metainfo file."
    )

    info_hash = models.BinaryField(
        blank=True,
        unique=True,
        help_text='The SHA1 hash of the info dict from the metainfo file.'
    )

    info_hash_hex = models.CharField(
        blank=True,
        max_length=100,
        unique=True,
        help_text='The hex-encoded info hash.'
    )

    @property
    def hexcode(self):
        return self.info_hash_hex[0:6]

    def clean(self):
        self.info_hash = utils.calculate_info_hash(str(self.raw_metainfo))
        self.info_hash_hex = binascii.hexlify(self.info_hash)

    def get_absolute_url(self):
        return reverse('register:torrent-detail', args=[self.info_hash_hex])


class File(models.Model):
    name = models.CharField(
        max_length=256,
        help_text='The name of the file, including the path.'
    )

    length = models.PositiveSmallIntegerField(
        help_text="The file's length, in bytes."
    )

    file_hash = models.CharField(
        blank=True,
        null=True,
        max_length=32,
        help_text="The file's MD5 hash (hex. encoded). Blank for directories."
    )

    torrent = models.ForeignKey(
        'register.Torrent'
    )

    @property
    def owning_directory(self):
        # wicked bad code!
        return '/'.join(self.name.split('/')[1:-1])

    @property
    def filename(self):
        return self.name.split('/')[-1]
